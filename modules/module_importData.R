library(DT)

importDataUI = function(id) {
  ns = NS(id)
  fluidPage(
    fixedRow(
      column(width = 3, 
        fileInput(
          ns("file_upload"), 
          "Выберите .ods / .xlsx  файл для загрузки", multiple = FALSE,
          accept = c(
            "application/vnd.oasis.opendocument.spreadshee",
            "application/excel",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            ".xlsx",
            ".ods"
          )
        )
      ),
      column(width = 8, ""),
      column( width = 1, hidden(actionButton(ns("action_btn_do_upload"), label = "Загрузить")))
    ),
    fluidRow(
      column(width = 12, div(withSpinner(DT::dataTableOutput(ns("ui_table_file_content"))), style = "font-size: 85%;"))
    )
  )
}

# New-style modules
importDataServer = function(id, dbcon = NULL) {
  moduleServer(
    id,
    function(input, output, session) {
      
      # reactives ---------------------------------------------------------------
      
      observe({
        req(input$file_upload)
        # browser()
        str = input$file_upload$datapath
        file_path_reactive_val(str)
        file_orig_name_reactive_val(input$file_upload$name)
      })  
      
      # reactives order_fields_dict_list_reactive ------------------------------------
      import_file_data_tbl_reactive = reactive({
        # browser()
        file_path = file_path_reactive_val()
        
        if (str_length(file_path) > 0 ) {
          if (tools::file_ext(file_path) == "xlsx") {
            file_data_tbl = read.xlsx(
              xlsxFile = file_path, sheet = 1, skipEmptyRows = TRUE, rowNames = FALSE,
              detectDates = TRUE
            )    
          }
          
          if (tools::file_ext(file_path) == "ods") {
            file_data_tbl = readODS::read_ods(
              path = file_path, sheet = 1
            ) %>% 
              mutate(Date_purchase = as.Date(Date_purchase, "%d.%m.%Y"))
          }
          
        } else {
          file_data_tbl = tibble(
            fz = character(),
            country_code = character(),
            region_code = character(),
            Date_purchase = as.Date(character()),
            purchase_name = character(),
            lot_price = numeric(),
            okpd2_code = character(),
            okpd2_names = character(),
            KTRU_code = character(),
            KTRU_name = character(),
            Description = character(),
            Date_TKP = as.Date(character()),
            TZ = character(),
            shablon = character()
          )
        }
        
        if (nrow(file_data_tbl) > 0) {
          # is_import_ready(TRUE)
          shinyjs::show("action_btn_do_upload")
        } else {
          shinyjs::hide("action_btn_do_upload")
        } 
        
        file_data_tbl
      })
      
      
      file_path_reactive_val = reactiveVal("")
      file_orig_name_reactive_val = reactiveVal("")
      is_import_ready = reactiveVal(FALSE)
      return_nrows_imported_reactive_val = reactiveVal()
      module_returns_reactive_vals = reactiveValues(nrows_imported = -1)
      
      # observers ---------------------------------------------------------------

      # observers action_btn_do_upload ----------------------------------------------------
      observeEvent(input$action_btn_do_upload, {
        # browser()
        
        # загрузка в БД
        import_file_data_tbl = isolate(import_file_data_tbl_reactive())
        # import_file_data_tbl = import_file_data_tbl %>% 
        #   mutate(
        #     KTRU_code = if_else(is.na(KTRU_code), "", as.character(KTRU_code)),
        #     KTRU_name = if_else(is.na(KTRU_name), "", as.character(KTRU_name)),
        #     Description = if_else(is.na(Description), "", as.character(Description))
        #   )
        import_file_orig_name = isolate(file_orig_name_reactive_val())
        
        now_loaded_dt = Sys.time()
        
        DBI::dbWithTransaction(dbcon, {
          imported_file_id <- DBI::dbGetQuery(
            dbcon,
            "INSERT INTO import_file_header (
                  file_orig_name,
                  loaded_dt
                ) VALUES ( $1 , $2) RETURNING file_id;",
            param = list(
              import_file_orig_name, now_loaded_dt
            )
          )
          
          rs <- DBI::dbSendStatement(
            dbcon,
            "INSERT INTO public.import_file_rows
                (file_id, fz, country_code, region_code, date_purchase, purchase_name, lot_price)
              VALUES( $1, $2, $3, $4, $5, $6, $7);",
            param = list(
              rep(imported_file_id$file_id, nrow(import_file_data_tbl)), 
              import_file_data_tbl$fz,
              import_file_data_tbl$country_code,
              import_file_data_tbl$region_code,
              import_file_data_tbl$Date_purchase,
              import_file_data_tbl$purchase_name,
              import_file_data_tbl$lot_price
            )
          )
          
          DBI::dbClearResult(rs)
          
          
        })
        
        file_path_reactive_val("")
        file_orig_name_reactive_val("")
        reset('file_upload')
        
        module_returns_reactive_vals$nrows_imported = nrow(import_file_data_tbl)
      }) # input$action_btn_do_upload

      # outputs -----------------------------------------------------------------

      output$ui_table_file_content = renderDT({
          
          # browser()
          
          data_tbl = import_file_data_tbl_reactive()
          
          datatable(
            data_tbl,
            class = "hover stripe",
            rownames = FALSE,
            escape = FALSE,
            selection = list(mode = "multiple", selected = c()),
            filter = 'none',
            autoHideNavigation = TRUE,
            options = list(
              language = list(url = '//cdn.datatables.net/plug-ins/1.10.11/i18n/Russian.json'),
              paging = nrow(data_tbl) > 5,
              searching = TRUE,
              info = TRUE,
              ordering = TRUE,
              responsive = TRUE,
              lengthMenu = c(5, 10,20,100),
              pageLength = 5,
              columnDefs = list(
              )
            )
            # ,
            # colnames = c(
            # )
          ) %>% 
          formatStyle(colnames(data_tbl), 'vertical-align'='top')
        }, 
        options = list(lengthChange = FALSE)
      )
      
      # module returns -----------------------------------------------------------
      return(module_returns_reactive_vals)
    }
  )
}